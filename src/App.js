import { useState } from "react";
import "./App.css";
import { ALL_CLUBS } from "./data/clubs-info/all/all_clubs";

function App() {
  const [clubs, setClubs] = useState([]);

  const [filterStars, setFilterStars] = useState([]);

  const numStars = filterStars.map((str) => str * 1);
  console.log(numStars);

  const getRandomElements = (array, count) => {
    const getRandomIndex = (max) => Math.floor(Math.random() * max);
    const usedIndexes = new Set();

    while (usedIndexes.size !== count) {
      usedIndexes.add(getRandomIndex(array.length));
    }

    return [...usedIndexes].map((index) => array[index]);
  };

  const filteredByStars = ALL_CLUBS.filter((club) =>
    numStars.includes(club.stars)
  );

  const handleRandom = (e) => {
    e.preventDefault();

    setClubs(getRandomElements(filteredByStars, 2));
  };
  const ClubBox = (props) => {
    if (props.club === undefined) {
      return;
    }

    return (
      <div
        className="random__box-img"
        style={{ backgroundImage: `url("${props.club.image}")` }}
      ></div>
    );
  };

  const starsHandler = (event) => {
    if (event.target.checked) {
      setFilterStars([...filterStars, event.target.value]);
    } else {
      setFilterStars(filterStars.filter((star) => star !== event.target.value));
    }
  };

  console.log(filteredByStars);

  return (
    <div className="App">
      <div className="logo"></div>
      <div className="random__wrapper">
        <div className="random__box-wrapper">
          <div className="random__box-item">
            <ClubBox club={clubs[0]} />
          </div>
          <div className="random__box-item">
            <ClubBox club={clubs[1]} />
          </div>
        </div>
      </div>
      <div className="random__panel">
        <form onSubmit={handleRandom}>
          <div className="input_wrapper">
            <label htmlFor="5-stars">
              <input
                type="checkbox"
                value={5}
                id="5-stars"
                onChange={starsHandler}
              />
              <span>5 stars</span>
            </label>
            <label htmlFor="4-5-stars">
              <input
                type="checkbox"
                value={4.5}
                id="4-5-stars"
                onChange={starsHandler}
              />
              <span>4.5 stars</span>
            </label>
            <label htmlFor="4-stars">
              <input
                type="checkbox"
                value={4}
                id="4-stars"
                onChange={starsHandler}
              />
              <span>4 stars</span>
            </label>
            <label htmlFor="3-5-stars">
              <input
                type="checkbox"
                value={3.5}
                id="3-5-stars"
                onChange={starsHandler}
              />
              <span>3.5 stars</span>
            </label>
            <label htmlFor="3-stars">
              <input
                type="checkbox"
                value={3}
                id="3-stars"
                onChange={starsHandler}
              />
              <span>3 stars</span>
            </label>
          </div>

          <button>Random</button>
        </form>
      </div>
    </div>
  );
}

export default App;
